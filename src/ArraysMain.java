import java.util.ArrayList;

public class ArraysMain {

    public static void main(String[] args) {

        ArrayList<Pet> myPetList = new ArrayList<Pet>();

        int[] intArray = {5, 32, 64, 34, 77};

        Pet[] petArray = new Pet[10];

        for(int i = 0; i< petArray.length;i++){
            System.out.println(petArray[i]);
        }

        Pet firstPet = new Pet();
        firstPet.setName("First Pet");

        petArray[0] = firstPet;

        Dragon myDragon = new Dragon();
        petArray[1] = myDragon;


    }

}
