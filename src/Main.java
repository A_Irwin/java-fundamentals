public class Main {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        System.out.println(myPet.numPets);

        myPet.setNumLegs(4);
        myPet.setName("Mycroft");


        System.out.println(myPet.getName());
        System.out.println(myPet.getNumLegs());

        myPet.feed();

        Dragon myDragon = new Dragon();
        myDragon.setNumLegs(4);
        myDragon.setName("David");


        System.out.println(myDragon.getName());
        System.out.println(myDragon.getNumLegs());

        myDragon.feed();
        myDragon.breatheFire();

    }
}
