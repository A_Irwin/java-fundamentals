import java.sql.SQLOutput;

public class Pet {

    public static int numPets = 0;

    private String name;
    private int numLegs;

    public Pet(){
        numPets++;
    }

    public void feed(){
        System.out.println("Feed pet");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }


}
